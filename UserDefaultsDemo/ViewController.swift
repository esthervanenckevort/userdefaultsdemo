//  Copyright © 2018 David van Enckevort
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import UIKit

class ViewController: UIViewController {

    @IBOutlet private var musicSwitch: UISwitch!
    @IBOutlet private var musicVolume: UISlider!
    @IBOutlet private var soundsSwitch: UISwitch!
    @IBOutlet private var soundsVolume: UISlider!
    private let musicController = MusicController()

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        musicSwitch.isOn = UserDefaults.standard.music
        soundsSwitch.isOn = UserDefaults.standard.sounds
        musicVolume.value = UserDefaults.standard.musicVolume
        soundsVolume.value = UserDefaults.standard.soundsVolume
    }

    @IBAction func toggleMusic(_ sender: UISwitch) {
        UserDefaults.standard.music = sender.isOn
    }
    @IBAction func updateMusicVolume(_ sender: UISlider) {
        UserDefaults.standard.musicVolume = sender.value
    }
    @IBAction func toggleSounds(_ sender: UISwitch) {
        UserDefaults.standard.sounds = sender.isOn
    }
    @IBAction func updateSoundsVolume(_ sender: UISlider) {
        UserDefaults.standard.soundsVolume = sender.value
    }
}
