//  Copyright © 2018 David van Enckevort
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import Foundation

class MusicController {

    private let musicObserver: NSKeyValueObservation
    private let musicVolumeObserver: NSKeyValueObservation
    private let soundsObserver: NSKeyValueObservation
    private let soundsVolumeObserver: NSKeyValueObservation
    
    init() {
        musicObserver = UserDefaults.standard.observe(\.music, options: [.new]) { (defaults, change) in
            guard let newValue = change.newValue else { return }
            print("Music switched \(newValue ? "on" : "off").")
        }
        musicVolumeObserver = UserDefaults.standard.observe(\.musicVolume, options: [.new]) { (defaults, change) in
            guard let newValue = change.newValue else { return }
            print("Music volume changed to \(newValue).")
        }
        soundsObserver = UserDefaults.standard.observe(\.sounds, options: [.new]) { (defaults, change) in
            guard let newValue = change.newValue else { return }
            print("Sounds effects switched \(newValue ? "on" : "off").")
        }
        soundsVolumeObserver = UserDefaults.standard.observe(\.soundsVolume, options: [.new]) { (defaults, change) in
            guard let newValue = change.newValue else { return }
            print("Sound effects volume changed to \(newValue).")
        }
    }
}
