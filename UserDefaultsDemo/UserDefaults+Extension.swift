//  Copyright © 2018 David van Enckevort
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import Foundation

extension UserDefaults {
    private enum Keys: String {
        case music, sounds, musicVolume, soundsVolume
    }

    @objc var music: Bool {
        get {
            return bool(forKey: Keys.music.rawValue)
        }
        set {
            set(newValue, forKey: Keys.music.rawValue)
        }
    }

    @objc var musicVolume: Float {
        get {
            return clamp(float(forKey: Keys.musicVolume.rawValue))
        }
        set {
            set(clamp(newValue), forKey: Keys.musicVolume.rawValue)
        }
    }

    @objc var sounds: Bool {
        get {
            return bool(forKey: Keys.sounds.rawValue)
        }
        set {
            set(newValue, forKey: Keys.sounds.rawValue)
        }
    }

    @objc var soundsVolume: Float {
        get {
            return clamp(float(forKey: Keys.soundsVolume.rawValue))
        }
        set {
            set(clamp(newValue), forKey: Keys.soundsVolume.rawValue)
        }
    }

    private func clamp(_ value: Float, to range: ClosedRange<Float> = 0.0...100.0) -> Float {
        return min(range.upperBound, max(range.lowerBound, value))
    }
}
